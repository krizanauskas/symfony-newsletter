# Symfony newsletter REST API

## Description

User:

Can subscribe to newsletter by providing email and newsletter category.
Get all available newsletter categories.

Admin:

Can login using email and password.
Can get all subscribers list, filter and sort it.
Delete specific subscriber

Admin authorizes using JSON Web Tokens.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Things you need to install the software and how to install them.
- PHP 7.2.5+
- [composer](https://getcomposer.org/download/)
- [symfony](https://symfony.com/doc/current/setup.html)
- docker (optional)

### Running app

```bash
git clone https://gitlab.com/krizanauskas/symfony-newsletter.git
cd symfony-newsletter
## edit .env if needed
composer install
symfony server:start
```
### Installing (alternative with Docker)

```bash
git clone https://gitlab.com/krizanauskas/symfony-newsletter.git
cd symfony-newsletter
## edit .env if needed
docker-compose build
docker-compose up
```

## Usage

Admin password and email can be changed in .env file

To hash new password, run command
```bash
php bin/console security:hash-password
```
To check documentation in OpenAPI (Swagger) format which provides a sandbox to interactively experiment with the API, open [http://127.0.0.1:8000/api/doc](http://127.0.0.1:8000/api/doc) or [http://symfony-newsletter.localhost/api/doc](http://symfony-newsletter.localhost/api/doc), depending on installation you chosen

## Testing

To run tests, execute following command:

```bash
php ./vendor/bin/phpunit
```
