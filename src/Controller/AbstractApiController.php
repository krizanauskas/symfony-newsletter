<?php

namespace App\Controller;

use Symfony\Component\Form\FormInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractFOSRestController
{
    protected function buildForm(string $type, $data = null, array $options = []): FormInterface
    {
        $options = array_merge($options, [
            'csrf_protection' => false,
        ]);

        return $this->container->get('form.factory')->createNamed('', $type, $data, $options);
    }

    protected function respond($data = null, int $statusCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view([
            "code" => $statusCode,
            "data" => $data
        ], $statusCode));
    }

    protected function formRespond($data, int $statusCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view($data, $statusCode));
    }

    protected function respondSuccess(int $statusCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view([
            "code" => $statusCode
        ], $statusCode));
    }
}
