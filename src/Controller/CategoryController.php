<?php

namespace App\Controller;

use App\Entity\Category;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends AbstractApiController
{
    /**
     * Get categories.
     *
     * This call returns list of available categories names.
     *
     * @OA\Get(
     *    path="/api/v1/categories"
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns categories names array",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         ),
     *         @OA\Property(
     *             property="data",
     *             type="array",
     *             @OA\Items(type="string")
     *         ),
     *     )
     * )
     * @OA\Tag(name="categories")
     */

    public function index(): Response
    {
        $categories = Category::AVAILABLE_CATEGORIES;

        return $this->respond($categories);
    }
}
