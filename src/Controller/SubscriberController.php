<?php

namespace App\Controller;

use App\Entity\Subscriber;
use App\Form\Type\SubscriberType;
use App\Repository\SubscriberRepository;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;

class SubscriberController extends AbstractApiController
{
    /**
     * List all subscribers.
     *
     * This call returns all subscribers which meets filter criteria.
     *
     * @OA\Get(
     *    path="/api/v1/subscribers"
     * )
     * @OA\Parameter(
     *     name="filter_value",
     *     in="query",
     *     description="category name to filter results",
     *     @OA\Schema(type="string")
     * )
     * * @OA\Parameter(
     *     name="sort_by",
     *     in="query",
     *     description="set sort by field",
     *     @OA\Schema(
     *         type="string",
     *         enum={"category", "email"},
     *         example=""
     *     )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns subscribers list",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         ),
     *         @OA\Property(
     *             property="data",
     *             type="array",
     *             @OA\Items(ref=@Model(type=Subscriber::class))
     *         ),
     *     )
     * )
     * @OA\Tag(name="subscribers")
     * @Security(name="Bearer")
     */

    public function index(SubscriberRepository $subscriberRepository, Request $request): Response
    {
        $filterValue = $request->query->get('filter_value', '');
        $sortBy = $request->query->get('sort_by', '');

        $subscribers = $subscriberRepository->findAll(Subscriber::DEFAULT_FILTER, $filterValue, $sortBy);

        return $this->respond($subscribers);
    }

    /**
     * Add a new subscriber.
     *
     * This call adds a new subscriber.
     *
     * @OA\Post(
     *    path="/api/v1/subscribers"
     * )
     * @OA\RequestBody(
     *    description="Subscriber object that will be created. Category name must be taken from 'categories' endpoint",
     *    required=true,
     *    @OA\JsonContent(
     *        @OA\Property( property="email", type="string"),
     *        @OA\Property( property="category", type="string")
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns created subscriber",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         ),
     *         @OA\Property(
     *             property="data",
     *             ref=@Model(type=Subscriber::class)
     *         ),
     *     )
     * )
     * @OA\Tag(name="subscribers")
     */

    public function store(Request $request, SubscriberRepository $subscriberRepository): Response
    {
        $form = $this->buildForm(SubscriberType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->formRespond($form, Response::HTTP_BAD_REQUEST);
        }

        $subscriberData = $form->getData();

        $subscriber = $subscriberRepository->store($subscriberData);

        return $this->respond($subscriber);
    }

    /**
     * Get subscriber.
     *
     * This call returns subscriber by id.
     *
     * @OA\Get(
     *    path="/api/v1/subscribers/{id}"
     * )
     * @OA\Parameter(
     *     description="ID of subscriber to return",
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Returns subscriber",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         ),
     *         @OA\Property(
     *             property="data",
     *             ref=@Model(type=Subscriber::class)
     *         ),
     *     )
     * )
     * @OA\Tag(name="subscribers")
     * @Security(name="Bearer")
     */

    public function show(int $id, SubscriberRepository $subscriberRepository): Response
    {
        $subscriber = $subscriberRepository->find($id);

        if (!$subscriber) {
            throw $this->createNotFoundException('Not found');
        }

        return $this->respond($subscriber);
    }

    /**
     * Update a subscriber.
     *
     * This call updates a subscriber by id.
     *
     * @OA\Put(
     *    path="/api/v1/subscribers/{id}"
     * )
     * @OA\Parameter(
     *     description="ID of subscriber to return",
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     * ),
     * @OA\RequestBody(
     *    description="Subscriber object that will be updated. Category name must be taken from 'categories' endpoint",
     *    required=true,
     *    @OA\JsonContent(
     *        @OA\Property( property="email", type="string"),
     *        @OA\Property( property="category", type="string")
     *    ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns updated subscriber",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         ),
     *         @OA\Property(
     *             property="data",
     *             ref=@Model(type=Subscriber::class)
     *         ),
     *     )
     * )
     * 
     * @OA\Tag(name="subscribers")
     * @Security(name="Bearer")
     */

    public function update(Request $request, int $id, SubscriberRepository $subscriberRepository): Response
    {
        $subscriber = $subscriberRepository->find($id);

        if (!$subscriber) {
            throw $this->createNotFoundException('Not found');
        }

        $form = $this->buildForm(SubscriberType::class, null, [
            'method' => $request->getMethod()
        ]);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->formRespond($form, Response::HTTP_BAD_REQUEST);
        }

        $subscriberData = $form->getData();

        $subscriber = $subscriberRepository->update($subscriberData, $id);

        return $this->respond($subscriber);
    }

    /**
     * Delete a subscriber.
     *
     * This call deletes a subscriber by id.
     *
     * @OA\Delete(
     *    path="/api/v1/subscribers/{id}"
     * )
     * @OA\Parameter(
     *     description="ID of subscriber to delete",
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     * ),
     * @OA\Response(
     *     response=200,
     *     description="",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="code",
     *             type="integer",
     *             example=200
     *         )
     *     )
     * )
     * @OA\Tag(name="subscribers")
     * @Security(name="Bearer")
     */

    public function destroy(int $id, SubscriberRepository $subscriberRepository): Response
    {
        $subscriber = $subscriberRepository->find($id);

        if (!$subscriber) {
            throw $this->createNotFoundException('Not found');
        }

        $subscriberRepository->destroy($id);

        return $this->respondSuccess();
    }
}
