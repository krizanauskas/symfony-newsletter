<?php

namespace App\Drivers;

interface DataDriverInterface
{
    public function loadFile(string $filename): self;
    public function getAll(): array;
    public function add($data): array;
    public function findById(int $id): ?array;
    public function update(array $data, int $id): ?array;
    public function destroy(int $id): bool;
    public function filterBy(string $column, string $value): self;
    public function sortBy(string $sortBy): self;
}
