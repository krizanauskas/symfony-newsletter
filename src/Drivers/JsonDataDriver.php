<?php

namespace App\Drivers;

use Exception;
use League\Flysystem\FilesystemOperator;
use League\Flysystem\UnableToReadFile;

class JsonDataDriver implements DataDriverInterface
{
    private $filename;
    private $databaseStorage;
    private $file = [];

    public function __construct(FilesystemOperator $databaseStorage)
    {
        $this->databaseStorage = $databaseStorage;
    }

    public function loadFile(string $filename): self
    {
        $this->filename = $filename;

        try {
            $file = json_decode($this->databaseStorage->read($this->filename), true);
        } catch (UnableToReadFile $ex) {
            $file = [];
        }

        $this->file = $file ? $file : [];

        return $this;
    }

    public function getAll(): array
    {
        return $this->file;
    }

    public function add($data): array
    {
        $data['id'] = $this->getIncrementId();

        $this->file[] = $data;

        $this->save();

        return $data;
    }

    public function findById(int $id): ?array
    {
        return $this->findBy('id', strval($id));
    }

    public function update(array $data, int $id): ?array
    {
        $index = $this->getIndexById($id);

        if ($index !== null) {

            $data['id'] = $id;
            $this->file[$index] = $data;

            $this->save();

            return $data;
        } else {
            return null;
        }
    }

    public function destroy(int $id): bool
    {
        $isDeleted = false;

        $index = $this->getIndexById($id);

        if ($index !== null) {
            array_splice($this->file, $index, 1);

            $this->save();

            $isDeleted = true;
        }

        return $isDeleted;
    }

    public function filterBy(string $column, string $value): self
    {
        if ($value && $this->file) {
            if (array_key_exists($column, $this->file[0])) {
                $this->file = array_values(array_filter($this->file, function ($record) use ($column, $value) {
                    return $record[$column] == $value;
                }));
            } else {
                throw new Exception('Bad filter field');
            }
        }

        return $this;
    }

    public function sortBy(string $sortBy): self
    {
        if ($sortBy && $this->file) {
            if (array_key_exists($sortBy, $this->file[0])) {
                usort($this->file, function ($item1, $item2) use ($sortBy) {
                    return $item1[$sortBy] <=> $item2[$sortBy];
                });
            } else {
                throw new Exception('Bad order field');
            }
        }

        return $this;
    }

    private function findBy(string $key, string $value): ?array
    {
        $file = $this->getAll();
        $index = array_search($value, array_column($file, $key));

        if ($index !== false) {
            return $file[$index];
        } else {
            return null;
        }
    }

    private function getIndexById(int $id): ?int
    {
        $file = $this->getAll();
        $index = array_search($id, array_column($file, 'id'));

        return $index !== false ? $index : null;
    }

    protected function save(): void
    {
        $this->databaseStorage->write($this->filename, $this->prepareFile());
    }

    private function prepareFile(): string
    {
        return json_encode($this->file);
    }

    private function getIncrementId(): int
    {
        $id = 1;
        $file = $this->getAll();

        if (count($file)) {
            $lastEl = end($file);
            $newId = $lastEl['id'] + 1;

            return $newId;
        }

        return $id;
    }
}
