<?php

namespace App\Entity;

class Category
{
    const CATEGORY_NEWS = 'news';
    const CATEGORY_TECH = 'tech';
    const CATEGORY_MARKETING = 'mmarketing';
    const CATEGORY_GAMES = 'games';

    const AVAILABLE_CATEGORIES = [
        self::CATEGORY_NEWS,
        self::CATEGORY_TECH,
        self::CATEGORY_MARKETING,
        self::CATEGORY_GAMES
    ];
}
