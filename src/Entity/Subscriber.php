<?php

namespace App\Entity;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(required={"email", "category"})
 */

class Subscriber
{
    const STORE_FILE_NAME = 'subscribers.json';

    const FILTER_CATEGORY = 'category';

    const SORT_BY_EMAIL = 'email';
    const SORT_BY_CATEGORY = 'category';

    const DEFAULT_FILTER = self::FILTER_CATEGORY;
    const DEFAULT_SORT = '';

    const AVAILABLE_FILTERS = [
        self::FILTER_CATEGORY
    ];
    const AVAILABLE_SORT_BY = [
        self::SORT_BY_EMAIL,
        self::SORT_BY_CATEGORY,
    ];

    /**
     * @var int
     * @OA\Property(description="The unique identifier of the subscriber.")
     */
    public $id;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public $email;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public $category;
}
