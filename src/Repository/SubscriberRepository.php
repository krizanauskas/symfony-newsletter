<?php

namespace App\Repository;

use App\Entity\Subscriber;
use App\Service\JsonDatabaseService;
use App\Drivers\DataDriverInterface;
use Doctrine\ORM\EntityRepository;

class SubscriberRepository
{
    protected $dataService;
    protected $filename = Subscriber::STORE_FILE_NAME;

    public function __construct(DataDriverInterface $dataDriver)
    {
        $this->dataDriver = $dataDriver;
        $this->dataDriver->loadFile($this->filename);
    }

    public function store(array $subscriber): array
    {
        $subscriber = $this->dataDriver->add($subscriber);

        return $subscriber;
    }

    public function findAll(string $filterBy, string $filterValue, string $sortBy): array
    {
        $subscribers = $this->dataDriver->filterBy($filterBy, $filterValue)->sortBy($sortBy)->getAll();

        return $subscribers;
    }

    public function find(int $id): ?array
    {
        $subscriber = $this->dataDriver->findByid($id);

        return $subscriber;
    }

    public function update(array $data, int $id): array
    {
        $subscriber = $this->dataDriver->update($data, $id);

        return $subscriber;
    }

    public function destroy(int $id): bool
    {
        $isDeleted = $this->dataDriver->destroy($id);

        return $isDeleted;
    }
}
