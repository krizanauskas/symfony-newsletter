<?php

namespace App\Tests;

use Exception;
use App\Drivers\JsonDataDriver;
use PHPUnit\Framework\TestCase;
use League\Flysystem\Filesystem;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;

final class JsonDataDriverTest extends TestCase
{
    private $filesystem;
    private $mockFilename = 'mock.json';
    private $mockRecord = [
        "id" => 1,
        "email" => "johndoe@example.com",
        "category" => "news"
    ];
    private $secondMockRecord = [
        "id" => 2,
        "email" => "johndoe@doe.com",
        "category" => "games"
    ];

    private function getFilesystem(): Filesystem
    {
        if (!$this->filesystem) {
            $this->filesystem = new Filesystem(new InMemoryFilesystemAdapter());
        }

        return $this->filesystem;
    }

    public function test_returns_empty_array_if_file_not_exists(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $this->assertEquals($dataStorage->getAll(), []);
    }

    public function test_loads_existing_file(): void
    {
        $this->getFilesystem()->write($this->mockFilename, json_encode([$this->mockRecord]));

        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $this->assertEquals($dataStorage->getAll(), [$this->mockRecord]);
    }

    public function test_adds_record(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);
        $this->assertEquals($dataStorage->getAll(), [$this->mockRecord]);
    }

    public function test_saves_record_to_file(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);

        $dataStorage_new = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);

        $this->assertEquals($dataStorage->getAll(), $dataStorage_new->getAll());
    }

    public function test_deletes_record_from_file(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);

        $this->assertEquals($dataStorage->getAll(), [$this->mockRecord]);

        $dataStorage->destroy(1);

        $this->assertEquals($dataStorage->getAll(), []);
    }

    public function test_finds_record_by_id(): void
    {
        $secondRecord = [
            "id" => 2,
            "email" => "johndoe@doe.com",
            "category" => "games"
        ];

        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);
        $dataStorage->add($secondRecord);

        $foundRecord = $dataStorage->findById(2);

        $this->assertEquals($secondRecord, $foundRecord);
    }

    public function test_updates_record(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);

        $dataStorage->update($this->secondMockRecord, 1);

        $updatedSecondRecord = $this->secondMockRecord;
        $updatedSecondRecord["id"] = 1;

        $this->assertEquals($dataStorage->getAll(), [$updatedSecondRecord]);
    }

    public function test_filters_by_column(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);
        $dataStorage->add($this->secondMockRecord);

        $filteredRecord = $dataStorage->filterBy('category', 'games')->getAll();

        $this->assertEquals($filteredRecord, [$this->secondMockRecord]);
    }

    public function test_sorts_by_column(): void
    {
        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);
        $dataStorage->add($this->secondMockRecord);

        $this->assertEquals($dataStorage->getAll(), [$this->mockRecord, $this->secondMockRecord]);

        $sortedData = $dataStorage->sortBy('category')->getAll();

        $this->assertEquals($sortedData, [$this->secondMockRecord, $this->mockRecord]);
    }

    public function test_filter_throws_exception_if_column_not_exist(): void
    {
        $this->expectException(Exception::class);

        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);

        $dataStorage->filterBy('non_existent', 'games');
    }

    public function test_sort_by_throws_exception_if_column_not_exist(): void
    {
        $this->expectException(Exception::class);

        $dataStorage = (new JsonDataDriver($this->getFilesystem()))->loadFile($this->mockFilename);
        $dataStorage->add($this->mockRecord);

        $dataStorage->sortBy('non_existent');
    }
}
